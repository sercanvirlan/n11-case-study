
module.exports = {
  // Project deployment base
  publicPath: process.env.NODE_ENV === 'production' ? '/n11-cs/' : '/'
}
